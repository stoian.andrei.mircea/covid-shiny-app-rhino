box::use(
  bs4Dash,
  fresh,
  reactable,
  shiny,
)
box::use(
  app / logic / utility,
)


#' @export
ui <- function(id) {
  ns <- shiny$NS(id)
  shiny$tagList(
    reactable$reactableOutput(ns("table"))
  )
  
  }
  


#' @export
server <- function(id, data) {
  shiny$moduleServer(id, function(input, output, session) {
    output$table <- reactable$renderReactable({
      utility$render_table(data())
      })
    })
  
}